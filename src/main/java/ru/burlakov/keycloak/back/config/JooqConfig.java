package ru.burlakov.keycloak.back.config;

import org.jooq.impl.DefaultConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JooqConfig {

    @Bean
    public DefaultConfiguration configuration() {
        return new DefaultConfiguration();
    }

}
