package ru.burlakov.keycloak.back.config;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

@Configuration
public class KeycloakConfig {

    @Value("${spring.security.keycloak.auth-url}")
    private String SERVER_URL;

    @Value("${spring.security.keycloak.realm}")
    private String REALM;

    @Value("${spring.security.keycloak.username}")
    private String USERNAME;

    @Value("${spring.security.keycloak.password}")
    private String PASSWORD;

    @Value("${spring.security.keycloak.client-id}")
    private String CLIENT_ID;

    @Value("${spring.security.keycloak.client-secret}")
    private String CLIENT_SECRET;

    private Keycloak keycloak;


    @PostConstruct
    public void setUp() {
        keycloak = KeycloakBuilder.builder()
                .serverUrl(SERVER_URL)
                .realm(REALM)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(CLIENT_ID)
                .clientSecret(CLIENT_SECRET)
                .username(USERNAME)
                .password(PASSWORD)
                .build();
    }

    @Bean
    @Primary
    public RealmResource realmResource() {
        return keycloak.realm(REALM);
    }

}
