package ru.burlakov.keycloak.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegDTO {
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
}
