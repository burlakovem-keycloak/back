package ru.burlakov.keycloak.back.mapper;

import org.mapstruct.Mapper;
import ru.burlakov.keycloak.back.dto.RegDTO;
import ru.burlakov.keycloak.back.jooq.tables.pojos.User;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    User regDTOToEntity(RegDTO dto);

}
