package ru.burlakov.keycloak.back.resource;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.burlakov.keycloak.back.dto.RegDTO;
import ru.burlakov.keycloak.back.dto.UserDTO;
import ru.burlakov.keycloak.back.jooq.tables.pojos.User;
import ru.burlakov.keycloak.back.service.UserService;

import javax.ws.rs.BadRequestException;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @PostMapping("/registration")
    public void registration(@RequestBody RegDTO regDTO) throws BadRequestException {
        userService.registration(regDTO);
    }

    @GetMapping("/test")
    public RegDTO test() {
        return new RegDTO();
    }

    @GetMapping("/all")
    public List<User> getAll() {
        return userService.getAll();
    }

}
