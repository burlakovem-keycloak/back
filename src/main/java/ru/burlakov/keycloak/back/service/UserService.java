package ru.burlakov.keycloak.back.service;

import lombok.AllArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import ru.burlakov.keycloak.back.dto.RegDTO;
import ru.burlakov.keycloak.back.jooq.tables.pojos.User;
import ru.burlakov.keycloak.back.mapper.UsersMapper;
import ru.burlakov.keycloak.back.repository.UserRepository;

import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private RealmResource realmResource;

    private UserRepository userRepository;

    private UsersMapper usersMapper;

    private final String USER_ROLE = "USER";

    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Transactional
    public void registration(RegDTO regDTO) throws BadRequestException {
        if(userRepository.findByUsername(regDTO.getUsername()).isPresent())
            throw new BadRequestException();

//        UsersResource userResource = realmResource.users();
//        UserRepresentation user = userResource.get("10f0009b-cc86-4c63-b8a5-3cb0cf843c04").toRepresentation();
//
//        int i = 0;

        UsersResource userResource = realmResource.users();
        UserRepresentation user = getUserRepresentation(regDTO);
        User entity = usersMapper.regDTOToEntity(regDTO);
        entity.setCreated(LocalDateTime.now());
        userRepository.save(entity);
    }

    public UserRepresentation getUserRepresentation(RegDTO regDTO) {
        RoleRepresentation roleRepresentation = realmResource.roles().get(USER_ROLE).toRepresentation();
        UserRepresentation user = new UserRepresentation();

        user.setEnabled(true);
        user.setUsername(regDTO.getUsername());
        user.setFirstName(regDTO.getFirstName());
        user.setLastName(regDTO.getLastName());
        user.setEmail(regDTO.getEmail());

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(true);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(regDTO.getUsername());
        user.setCredentials(Collections.singletonList(credentialRepresentation));

//        user.setClientRoles(new HashMap<String, List<String>>() {
//            {
//                put("")
//            }
//        });

        return user;
    }

}
