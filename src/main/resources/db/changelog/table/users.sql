CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR UNIQUE,
    password VARCHAR,
    created TIMESTAMP,
    email VARCHAR,
    first_name VARCHAR,
    last_name VARCHAR
);
